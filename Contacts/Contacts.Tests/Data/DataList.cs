﻿using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Tests.Data
{
    class DataList
    {
        public List<IEntityContact> Contacts { get; set; }

        public DataList()
        {
            FillList();
        }

        private void FillList()
        {
            Contacts = new List<IEntityContact>
            {
                new RepoContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg"),
                new RepoContact(1, "Obi-Wan Kenobi", "+380633213210", "https://image.ibb.co/e2ftad/Obi_Wan_min.jpg", "https://image.ibb.co/crGo8y/ObiWan.jpg"),
                new RepoContact(2, "Qui-Gon Jinn", "+380633213210", "https://image.ibb.co/drkFoy/Qui_Gon_min.jpg", "https://image.ibb.co/b3V4gJ/QuiGon.jpg"),
                new RepoContact(3, "Mace Windu", "+380633213210", "https://image.ibb.co/mAuKFd/Mace_Windu_min.jpg", "https://image.ibb.co/ezagTy/Mace_Windu.jpg"),
                new RepoContact(4, "Luke Skywalker", "+380633213210", "https://image.ibb.co/ex26vd/Luke_min.jpg", "https://image.ibb.co/nJjmvd/Luke.jpg")
            };
        }
    }
}
