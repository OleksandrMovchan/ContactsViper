﻿using Contacts.Tests.Data;
using Contacts.Viper.ContactsEdit;
using Contacts.Viper.ContactsEdit.Interfaces;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Tests.Viper.ContactsEdit
{
    [TestFixture]
    public class TestInteractorEdit
    {

        [Test]
        public void InteractorEditConstructor_DataExists()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            Assert.DoesNotThrow(() =>
            {
                IInteractorEdit interactorInfo = new InteractorEdit(mockDataWrap.Object);
            });
        }

        [Test]
        public void InteractorInfoConstructor_DataNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IInteractorEdit interactorInfo = new InteractorEdit(null);
            });
        }

        [Test]
        public void SetId_TestCall()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorEdit interactorInfo = new InteractorEdit(mockDataWrap.Object);
            Mock<IPresenterEdit> mockPresenterInfo = new Mock<IPresenterEdit>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityEdit>()));
            mockDataWrap.Setup(f => f.GetDataById(It.IsAny<int>())).Returns(mockEntity.Object);

            interactorInfo.Presenter = mockPresenterInfo.Object;

            interactorInfo.SetId(id);
            mockPresenterInfo.Verify(f => f.SetData(It.IsAny<IEntityEdit>()), Times.Once);
        }

        [Test]
        public void SetId_TestData()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            IInteractorEdit interactorInfo = new InteractorEdit(dataWrapper);
            Mock<IPresenterEdit> mockPresenterInfo = new Mock<IPresenterEdit>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityEdit>()));

            interactorInfo.Presenter = mockPresenterInfo.Object;

            interactorInfo.SetId(id);

            IEntityEdit expected = new EntityEdit();
            expected.Id = id;
            expected.Name = "Obi-Wan Kenobi";
            expected.Number = "+380633213210";

            mockPresenterInfo.Verify(f => f.SetData(expected), Times.Once);
        }

        [Test]
        public void Id_Vefiry()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            InteractorEdit interactor = new InteractorEdit(mockDataWrap.Object);

            int id = 14;

            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IPresenterEdit> mockPresenterInfo = new Mock<IPresenterEdit>(MockBehavior.Strict);
            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityEdit>()));
            mockDataWrap.Setup(f => f.GetDataById(id)).Returns(mockEntity.Object);

            interactor.Presenter = mockPresenterInfo.Object;
            interactor.SetId(id);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            FieldInfo fieldId = interactorType.GetField("_id", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            Assert.AreEqual(id, (int)fieldId.GetValue(interactor), "Id set incorrectly");
        }

        [Test]
        public void SetPresenter_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorEdit interactor = new InteractorEdit(mockDataWrap.Object);

            Mock<IPresenterEdit> mockPresenterInfo = new Mock<IPresenterEdit>(MockBehavior.Loose);
            interactor.Presenter = mockPresenterInfo.Object;
            Assert.AreEqual(mockPresenterInfo.Object, interactor.Presenter, "presenter setted incorrect");
        }

        [Test]
        public void SetPresenter_Null()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            IInteractorEdit interactor = new InteractorEdit(mockDataWrap.Object);

            Assert.Throws<ArgumentNullException>(() =>
            {
                interactor.Presenter = null;
            });
        }

        [Test]
        public void ConvertData_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorEdit interactor = new InteractorEdit(mockDataWrap.Object);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodConvertData = interactorType.GetMethod("ConvertData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            IEntityContact entityContact = new RepoContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg");
            object[] mcdArgs = new object[]
            {
                entityContact
            };

            EntityEdit result = (EntityEdit)methodConvertData.Invoke(interactor, mcdArgs);
            EntityEdit expected = new EntityEdit();
            expected.Id = 0;
            expected.Name = "Master Yoda";
            expected.Number = "+380633213210";

            Assert.AreEqual(expected, result, "Data converted incorrectly");
        }

        [Test]
        public void GetData_Test()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            InteractorEdit interactor = new InteractorEdit(dataWrapper);

            int id = 0;

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodGetData = interactorType.GetMethod("GetData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            object[] mcdArgs = new object[]
            {
                id
            };

            EntityEdit result = (EntityEdit)methodGetData.Invoke(interactor, mcdArgs);
            EntityEdit expected = new EntityEdit();
            expected.Id = 0;
            expected.Name = "Master Yoda";
            expected.Number = "+380633213210";

            Assert.AreEqual(expected, result, "Data got incorrectly");
        }
    }
}

