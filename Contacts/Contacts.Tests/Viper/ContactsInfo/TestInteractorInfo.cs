﻿using Contacts.Tests.Data;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Reflection;

namespace Contacts.Tests.Viper.ContactsInfo
{
    [TestFixture]
    public class TestInteractorInfo
    {
        [Test]
        public void InteractorInfoConstructor_DataExists()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            Assert.DoesNotThrow(() =>
            {
                IInteractorInfo interactorInfo = new InteractorInfo(mockDataWrap.Object);
            });
        }

        [Test]
        public void InteractorInfoConstructor_DataNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IInteractorInfo interactorInfo = new InteractorInfo(null);
            });
        }

        [Test]
        public void SetId_TestCall()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorInfo interactorInfo = new InteractorInfo(mockDataWrap.Object);
            Mock<IPresenterInfo> mockPresenterInfo = new Mock<IPresenterInfo>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityInfo>()));
            mockDataWrap.Setup(f => f.GetDataById(It.IsAny<int>())).Returns(mockEntity.Object);

            interactorInfo.Presenter = mockPresenterInfo.Object;

            interactorInfo.SetId(id);
            mockPresenterInfo.Verify(f => f.SetData(It.IsAny<IEntityInfo>()), Times.Once);
        }

        [Test]
        public void SetId_TestData()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            IInteractorInfo interactorInfo = new InteractorInfo(dataWrapper);
            Mock<IPresenterInfo> mockPresenterInfo = new Mock<IPresenterInfo>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityInfo>()));

            interactorInfo.Presenter = mockPresenterInfo.Object;

            interactorInfo.SetId(id);

            EntityInfo expected = new EntityInfo("Obi-Wan Kenobi", "+380633213210", "https://image.ibb.co/e2ftad/Obi_Wan_min.jpg");
            mockPresenterInfo.Verify(f => f.SetData(expected), Times.Once);
        }

        [Test]
        public void Id_Vefiry()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            InteractorInfo interactor = new InteractorInfo(mockDataWrap.Object);

            int id = 14;

            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IPresenterInfo> mockPresenterInfo = new Mock<IPresenterInfo>(MockBehavior.Strict);
            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityInfo>()));
            mockDataWrap.Setup(f => f.GetDataById(id)).Returns(mockEntity.Object);

            interactor.Presenter = mockPresenterInfo.Object;
            interactor.SetId(id);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            FieldInfo fieldId = interactorType.GetField("_id", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            Assert.AreEqual(id, (int)fieldId.GetValue(interactor), "Id set incorrectly");
        }

        [Test]
        public void SetPresenter_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorInfo interactor = new InteractorInfo(mockDataWrap.Object);

            Mock<IPresenterInfo> mockPresenterInfo = new Mock<IPresenterInfo>(MockBehavior.Loose);
            interactor.Presenter = mockPresenterInfo.Object;
            Assert.AreEqual(mockPresenterInfo.Object, interactor.Presenter, "presenter setted incorrect");
        }

        [Test]
        public void SetPresenter_Null()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorInfo interactor = new InteractorInfo(mockDataWrap.Object);

            Assert.Throws<ArgumentNullException>(() =>
            {
                interactor.Presenter = null;
            });
        }

        [Test]
        public void ConvertData_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorInfo interactor = new InteractorInfo(mockDataWrap.Object);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodConvertData = interactorType.GetMethod("ConvertData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            IEntityContact entityContact = new RepoContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg");
            object[] mcdArgs = new object[]
            {
                entityContact
            };

            EntityInfo result = (EntityInfo)methodConvertData.Invoke(interactor, mcdArgs);
            EntityInfo expected = new EntityInfo("Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg");

            Assert.AreEqual(expected, result, "Data converted incorrectly");
        }

        [Test]
        public void GetData_Test()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            InteractorInfo interactor = new InteractorInfo(dataWrapper);

            int id = 0;

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodGetData = interactorType.GetMethod("GetData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            object[] mcdArgs = new object[]
            {
                
            };

            EntityInfo result = (EntityInfo)methodGetData.Invoke(interactor, mcdArgs);
            EntityInfo expected = new EntityInfo("Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg");

            Assert.AreEqual(expected, result, "Data got incorrectly");
        }
    }
}
