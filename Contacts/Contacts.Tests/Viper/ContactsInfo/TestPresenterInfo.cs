﻿using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Tests.Viper.ContactsInfo
{
    [TestFixture]
    public class TestPresenterInfo
    {
        [Test]
        public void PresenterInfoConstructor_ArgsExists()
        {
            Mock<IViewInfo> mockViewContact = new Mock<IViewInfo>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.DoesNotThrow(() =>
            {
                IPresenterInfo presenter = new PresenterInfo(mockViewContact.Object, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterInfoConstructor_ViewIsNull()
        {
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterInfo presenter = new PresenterInfo(null, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterInfoConstructor_RouterIsNull()
        {
            Mock<IViewInfo> mockViewContact = new Mock<IViewInfo>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterInfo presenter = new PresenterInfo(mockViewContact.Object, null);
            });
        }

        [Test]
        public void PresenterInfoConstructor_ArgsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterInfo presenter = new PresenterInfo(null, null);
            });
        }

        [Test]
        //Test that Presenter calls Router's method GoContactInfo(int id);
        //Sent id in this test doesn't matter
        public void GoBack_TestCall()
        {
            Mock<IViewInfo> mockViewContact = new Mock<IViewInfo>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterInfo presenter = new PresenterInfo(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoBack());
            presenter.BackClicked();

            mockRouter.Verify(f => f.GoBack(), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetName()
        {
            Mock<IViewInfo> mockViewInfo = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetNumber(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetNumber()
        {
            Mock<IViewInfo> mockViewInfo = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetPhoto(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetPhoto()
        {
            Mock<IViewInfo> mockViewInfo = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }


        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetName()
        {
            Mock<IViewInfo> mockViewInfo = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Name).Returns(data);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetName(data), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetNumber(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetNumber()
        {
            Mock<IViewInfo> mockViewInfo = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Number).Returns(data);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetNumber(data), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetPhoto(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetPhoto()
        {
            Mock<IViewInfo> mockViewContact = new Mock<IViewInfo>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterInfo presenter = new PresenterInfo(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IEntityInfo> mockEntity = new Mock<IEntityInfo>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Photo).Returns(data);

            presenter.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetPhoto(data), Times.Once);
        }
    }
}
