﻿using Contacts.Router;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Moq;
using NUnit.Framework;
using System;

namespace Contacts.Tests.Viper.ContactsList
{
    [TestFixture]
    public class TestPresenterContact
    {
        [Test]
        public void PresenterContact_AgrsExists()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.DoesNotThrow(() =>
            {
                IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterContact_ViewIsNull()
        {
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterContact presenterContact = new PresenterContact(null, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterContact_RouterIsNull()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, null);
            });
        }

        [Test]
        public void PresenterContact_ArgsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterContact presenterContact = new PresenterContact(null, null);
            });
        }

        [Test]
        //Test that Presenter calls Router's method GoContactInfo(int id);
        //Sent id in this test doesn't matter
        public void GoToContactInfo_TestCall()
        {
            int id = 4;
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoToContactInfo(It.IsAny<int>()));

            presenterContact.GoToContactInfo(id);

            mockRouter.Verify(f => f.GoToContactInfo(It.IsAny<int>()), Times.Once); 
        }

        [Test]
        //Test that Presenter calls Router's method GoContactInfo(int id);
        //Send id and verify it
        public void GoToContactInfo_TestId()
        {
            int id = 4;
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoToContactInfo(It.IsAny<int>()));

            presenterContact.GoToContactInfo(id);

            mockRouter.Verify(f => f.GoToContactInfo(id), Times.Once);
        }

        [Test]
        //Test that Presenter calls Router's method GoPhoto(int id);
        //Sent id in this test doesn't matter
        public void GoToPhoto_TestCall()
        {
            int id = 4;
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoToPhoto(It.IsAny<int>()));

            presenterContact.GoToPhoto(id);

            mockRouter.Verify(f => f.GoToPhoto(It.IsAny<int>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls Router's method GoPhoto(int id);
        //Send id and verify it
        public void GoToPhoto_TestId()
        {
            int id = 4;
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoToPhoto(It.IsAny<int>()));

            presenterContact.GoToPhoto(id);

            mockRouter.Verify(f => f.GoToPhoto(id), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetName()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetNumber(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetNumber()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetPhoto(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetPhoto()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetName(It.IsAny<string>()), Times.Once);
        }


        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetName()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Name).Returns(data);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetName(data), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetNumber(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetNumber()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Number).Returns(data);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetNumber(data), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetPhoto(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetPhoto()
        {
            Mock<IViewContact> mockViewContact = new Mock<IViewContact>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterContact presenterContact = new PresenterContact(mockViewContact.Object, mockRouter.Object);

            mockViewContact.Setup(f => f.SetName(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetNumber(It.IsAny<string>()));
            mockViewContact.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IUserEntityContact> mockEntity = new Mock<IUserEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Photo).Returns(data);

            presenterContact.SetData(mockEntity.Object);

            mockViewContact.Verify(f => f.SetPhoto(data), Times.Once);
        }
    }
}
