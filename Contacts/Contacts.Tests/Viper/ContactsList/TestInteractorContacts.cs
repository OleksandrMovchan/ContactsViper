﻿using Contacts.Tests.Data;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Reflection;

namespace Contacts.Tests.Viper.ContactsList
{
    [TestFixture]
    public class TestInteractorContacts
    {
        [Test]
        public void InteractorContacts_ArgExists()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            Assert.DoesNotThrow(() =>
            {
                IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object);
            });
        }

        [Test]
        public void InteractorContacts_ArgNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IInteractorContact interactorContact = new InteractorContact(null);
            });
        }

        [Test]
        //Test that Interactor calls Presenter's method GoToContactInfo(int id);
        //Sent id in this test doesn't matter
        public void CardClicked_TestCall()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict); //Create Mock-object of DataWrapper with Any data inside
            IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object); //Create real instance of interactor
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict); //Create Mock-object presenrter

            mockPresenterContact.Setup(f => f.GoToContactInfo(It.IsAny<int>())); //Setup that Mock-object of presenter has method GoToContactInfo with Any int in parameter

            interactorContact.Presenter = mockPresenterContact.Object; //Set the property in Interactor by mock-presenter

            interactorContact.CardClicked(); //Call the method CardClicked
            mockPresenterContact.Verify(f => f.GoToContactInfo(It.IsAny<int>()), Times.Once); //Verifiing that method GoToContactInfo was called once in presenter
        }

        [Test]
        //Test that Interactor calls Presenter's method GoToContactInfo(int id);
        //Uses mock-object of data
        public void CardClicked_TestData()
        {
            int position = 1;
            int id = 1;

            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);  //Creating Mock-object of entity that doesn't throw any exception after call
            mockEntity.SetupGet(f => f.Id).Returns(id);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object); 
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict); 

            mockPresenterContact.Setup(f => f.GoToContactInfo(It.IsAny<int>()));
            mockPresenterContact.Setup(f => f.SetData(It.IsAny<IUserEntityContact>())); //Setup that Mock-object of presenter has method SetData with Any IUserEntityContact in parameter
            mockDataWrap.Setup(f => f.GetDataByPosition(position)).Returns(mockEntity.Object); //Setup that Mock-object of DataWrapper has method GetData with our id in parameter and returns our mock-entity

            interactorContact.Presenter = mockPresenterContact.Object;
            interactorContact.SetId(position); //Call the method Set Id to verify
            interactorContact.CardClicked();
            mockPresenterContact.Verify(f => f.GoToContactInfo(id), Times.Once); //Verifiing that method GoToContactInfo was called once in presenter and pass our id
        }

        [Test]
        //Test that Interactor calls Presenter's method GoToPhoto(int id);
        //Sent id in this test doesn't matter
        public void AvatarClicked_TestCall()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);

            mockPresenterContact.Setup(f => f.GoToPhoto(It.IsAny<int>()));

            interactorContact.Presenter = mockPresenterContact.Object;

            interactorContact.AvatarClicked();
            mockPresenterContact.Verify(f => f.GoToPhoto(It.IsAny<int>()), Times.Once);
        }

        [Test]
        //Test that Interactor calls Presenter's method GoToPhoto(int id);
        //Uses mock-object of data
        public void AvatarClicked_TestData()
        {
            int position = 1;
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Id).Returns(id);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);

            mockPresenterContact.Setup(f => f.GoToPhoto(It.IsAny<int>()));
            mockPresenterContact.Setup(f => f.SetData(It.IsAny<IUserEntityContact>())); 
            mockDataWrap.Setup(f => f.GetDataByPosition(position)).Returns(mockEntity.Object); 

            interactorContact.Presenter = mockPresenterContact.Object;
            interactorContact.SetId(position); 
            interactorContact.AvatarClicked();
            mockPresenterContact.Verify(f => f.GoToPhoto(id), Times.Once); 
        }

        [Test]
        //Test that Interactor calls Presenter's method SetData(IUserEntityContact contact);
        //Sent id in this test doesn't matter
        public void SetId_TestCall()
        {
            int position = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorContact interactorContact = new InteractorContact(mockDataWrap.Object);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);

            mockPresenterContact.Setup(f => f.SetData(It.IsAny<IUserEntityContact>()));
            mockDataWrap.Setup(f => f.GetDataByPosition(It.IsAny<int>())).Returns(mockEntity.Object);

            interactorContact.Presenter = mockPresenterContact.Object;

            interactorContact.SetId(position);
            mockPresenterContact.Verify(f => f.SetData(It.IsAny<IUserEntityContact>()), Times.Once);
        }
        
        [Test]
        //Test that Interactor calls Presenter's method SetData(IUserEntityContact contact);
        //Uses mock-object of data
        public void SetId_TestData()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            IInteractorContact interactorContact = new InteractorContact(dataWrapper);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);

            mockPresenterContact.Setup(f => f.SetData(It.IsAny<IUserEntityContact>()));

            interactorContact.Presenter = mockPresenterContact.Object;

            interactorContact.SetId(id);

            UserContact expected  = new UserContact(1, "Obi-Wan Kenobi", "+380633213210", "https://image.ibb.co/e2ftad/Obi_Wan_min.jpg");
            mockPresenterContact.Verify(f => f.SetData(expected), Times.Once);
        }

        [Test]
        public void Id_Vefiry()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            InteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            int position = 14;
            int id = 14;

            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Id).Returns(id);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);
            mockPresenterContact.Setup(f => f.SetData(It.IsAny<IUserEntityContact>()));
            mockDataWrap.Setup(f => f.GetDataByPosition(id)).Returns(mockEntity.Object);
            
            interactor.Presenter = mockPresenterContact.Object;
            interactor.SetId(position);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            FieldInfo fieldId = interactorType.GetField("_id", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance
            
            Assert.AreEqual(id, (int) fieldId.GetValue(interactor), "Id set incorrectly");
        }

        [Test]
        public void OnImagePress_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);
            IInteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            interactor.Presenter = mockPresenterContact.Object;
            mockPresenterContact.Setup(a => a.GoToPhoto(It.IsAny <int>()));
            mockPresenterContact.Raise(a => a.OnImagePress += null);

            mockPresenterContact.Verify(f => f.GoToPhoto(It.IsAny <int>()), Times.Once);
        }

        [Test]
        public void OnCardPress_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Strict);
            IInteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            interactor.Presenter = mockPresenterContact.Object;
            mockPresenterContact.Setup(a => a.GoToContactInfo(It.IsAny<int>()));
            mockPresenterContact.Raise(a => a.OnCardPress += null);

            mockPresenterContact.Verify(f => f.GoToContactInfo(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void SetPresenter_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            Mock<IPresenterContact> mockPresenterContact = new Mock<IPresenterContact>(MockBehavior.Loose);
            interactor.Presenter = mockPresenterContact.Object;
            Assert.AreEqual(mockPresenterContact.Object, interactor.Presenter, "presenter setted incorrect");
        }

        [Test]
        public void SetPresenter_Null()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            Assert.Throws<ArgumentNullException>(() =>
            {
                interactor.Presenter = null;
            });
        }

        [Test]
        public void ConvertData_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorContact interactor = new InteractorContact(mockDataWrap.Object);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodConvertData = interactorType.GetMethod("ConvertData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            IEntityContact entityContact = new RepoContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg");
            object[] mcdArgs = new object[]
            {
                entityContact
            };

            UserContact result = (UserContact) methodConvertData.Invoke(interactor, mcdArgs);
            UserContact expected = new UserContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg");

            Assert.AreEqual(expected, result, "Data converted incorrectly");
        }

        [Test]
        public void GetData_Test()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            InteractorContact interactor = new InteractorContact(dataWrapper);
            int id = 0;

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodGetData = interactorType.GetMethod("GetData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            object[] mcdArgs = new object[]
            {
                id
            };

            UserContact result = (UserContact)methodGetData.Invoke(interactor, mcdArgs);
            UserContact expected = new UserContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg");

            Assert.AreEqual(expected, result, "Data got incorrectly");
        }
    }
}
