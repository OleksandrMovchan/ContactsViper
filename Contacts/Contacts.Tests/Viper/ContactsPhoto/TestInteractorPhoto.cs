﻿using Contacts.Tests.Data;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Tests.Viper.ContactsPhoto
{
    [TestFixture]
    public class TestInteractorPhoto
    {
        [Test]
        public void InteractorPhotoConstructor_DataExists()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            Assert.DoesNotThrow(() =>
            {
                IInteractorPhoto interactorPhoto = new InteractorPhoto(mockDataWrap.Object);
            });
        }

        [Test]
        public void InteractorPhotoConstructor_DataNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IInteractorPhoto interactorPhoto = new InteractorPhoto(null);
            });
        }

        [Test]
        public void SetId_TestCall()
        {
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorPhoto interactorPhoto = new InteractorPhoto(mockDataWrap.Object);
            Mock<IPresenterPhoto> mockPresenterInfo = new Mock<IPresenterPhoto>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityPhoto>()));
            mockDataWrap.Setup(f => f.GetDataById(It.IsAny<int>())).Returns(mockEntity.Object);

            interactorPhoto.Presenter = mockPresenterInfo.Object;

            interactorPhoto.SetId(id);
            mockPresenterInfo.Verify(f => f.SetData(It.IsAny<IEntityPhoto>()), Times.Once);
        }

        [Test]
        public void SetId_TestData()
        {
            int position = 1;
            int id = 1;
            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Id).Returns(id);
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            IInteractorPhoto interactorInfo = new InteractorPhoto(dataWrapper);
            Mock<IPresenterPhoto> mockPresenterInfo = new Mock<IPresenterPhoto>(MockBehavior.Strict);

            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityPhoto>()));

            interactorInfo.Presenter = mockPresenterInfo.Object;

            interactorInfo.SetId(position);

            EntityPhoto expected = new EntityPhoto(id, "https://image.ibb.co/crGo8y/ObiWan.jpg");
            mockPresenterInfo.Verify(f => f.SetData(expected), Times.Once);
        }

        [Test]
        public void Id_Vefiry()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Strict);
            IInteractorPhoto interactor = new InteractorPhoto(mockDataWrap.Object);

            int position = 14;
            int id = 14;

            Mock<IEntityContact> mockEntity = new Mock<IEntityContact>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Id).Returns(id);
            Mock<IPresenterPhoto> mockPresenterInfo = new Mock<IPresenterPhoto>(MockBehavior.Strict);
            mockPresenterInfo.Setup(f => f.SetData(It.IsAny<IEntityPhoto>()));
            mockDataWrap.Setup(f => f.GetDataById(position)).Returns(mockEntity.Object);

            interactor.Presenter = mockPresenterInfo.Object;
            interactor.SetId(id);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            FieldInfo fieldId = interactorType.GetField("_id", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            Assert.AreEqual(id, (int)fieldId.GetValue(interactor), "Id set incorrectly");
        }

        [Test]
        public void SetPresenter_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            IInteractorPhoto interactor = new InteractorPhoto(mockDataWrap.Object);

            Mock<IPresenterPhoto> mockPresenterInfo = new Mock<IPresenterPhoto>(MockBehavior.Loose);
            interactor.Presenter = mockPresenterInfo.Object;
            Assert.AreEqual(mockPresenterInfo.Object, interactor.Presenter, "presenter setted incorrect");
        }

        [Test]
        public void SetPresenter_Null()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            IInteractorPhoto interactor = new InteractorPhoto(mockDataWrap.Object);

            Assert.Throws<ArgumentNullException>(() =>
            {
                interactor.Presenter = null;
            });
        }

        [Test]
        public void ConvertData_Test()
        {
            Mock<IDataWrapper> mockDataWrap = new Mock<IDataWrapper>(MockBehavior.Loose);
            InteractorPhoto interactor = new InteractorPhoto(mockDataWrap.Object);

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodConvertData = interactorType.GetMethod("ConvertData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            IEntityContact entityContact = new RepoContact(0, "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg");
            object[] mcdArgs = new object[]
            {
                entityContact
            };

            EntityPhoto result = (EntityPhoto)methodConvertData.Invoke(interactor, mcdArgs);
            EntityPhoto expected = new EntityPhoto(0, "https://image.ibb.co/efDqMJ/Yoda.jpg");

            Assert.AreEqual(expected, result, "Data converted incorrectly");
        }

        [Test]
        public void GetData_Test()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            InteractorPhoto interactor = new InteractorPhoto(dataWrapper);

            //int id = 0;

            Type interactorType = interactor.GetType(); //Get the Type of interactor's instance
            MethodInfo methodGetData = interactorType.GetMethod("GetData", BindingFlags.Instance | BindingFlags.NonPublic); //Get the field of interactor's instance

            object[] mcdArgs = new object[]
            {

            };

            EntityPhoto result = (EntityPhoto)methodGetData.Invoke(interactor, mcdArgs);
            EntityPhoto expected = new EntityPhoto(0, "https://image.ibb.co/efDqMJ/Yoda.jpg");

            Assert.AreEqual(expected, result, "Data got incorrectly");
        }
    }
}
