﻿using Contacts.Router;
using Contacts.Viper.ContactsPhoto;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Moq;
using NUnit.Framework;
using System;

namespace Contacts.Tests.Viper.ContactsPhoto
{
    [TestFixture]
    public class TestPresenterPhoto
    {
        [Test]
        public void PresenterInfoConstructor_ArgsExists()
        {
            Mock<IViewPhoto> mockViewContact = new Mock<IViewPhoto>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.DoesNotThrow(() =>
            {
                IPresenterPhoto presenter = new PresenterPhoto(mockViewContact.Object, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterInfoConstructor_ViewIsNull()
        {
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterPhoto presenter = new PresenterPhoto(null, mockRouter.Object);
            });
        }

        [Test]
        public void PresenterInfoConstructor_RouterIsNull()
        {
            Mock<IViewPhoto> mockViewContact = new Mock<IViewPhoto>(MockBehavior.Loose);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterPhoto presenter = new PresenterPhoto(mockViewContact.Object, null);
            });
        }

        [Test]
        public void PresenterInfoConstructor_ArgsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                IPresenterPhoto presenter = new PresenterPhoto(null, null);
            });
        }

        [Test]
        //Test that Presenter calls Router's method GoContactInfo(int id);
        //Sent id in this test doesn't matter
        public void GoBack_TestCall()
        {
            Mock<IViewPhoto> mockViewContact = new Mock<IViewPhoto>(MockBehavior.Loose);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Strict);

            IPresenterPhoto presenter = new PresenterPhoto(mockViewContact.Object, mockRouter.Object);

            mockRouter.Setup(f => f.GoBack());
            presenter.BackClicked();

            mockRouter.Verify(f => f.GoBack(), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify call
        public void SetData_TestCall_SetPhoto()
        {
            Mock<IViewPhoto> mockViewInfo = new Mock<IViewPhoto>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterPhoto presenter = new PresenterPhoto(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            Mock<IEntityPhoto> mockEntity = new Mock<IEntityPhoto>(MockBehavior.Loose);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetPhoto(It.IsAny<string>()), Times.Once);
        }

        [Test]
        //Test that Presenter calls View's method SetName(int id);
        //Send mock-entity and verify data
        public void SetData_TestData_SetName()
        {
            Mock<IViewPhoto> mockViewInfo = new Mock<IViewPhoto>(MockBehavior.Strict);
            Mock<IRouter> mockRouter = new Mock<IRouter>(MockBehavior.Loose);

            IPresenterPhoto presenter = new PresenterPhoto(mockViewInfo.Object, mockRouter.Object);

            mockViewInfo.Setup(f => f.SetPhoto(It.IsAny<string>()));

            string data = "some data";
            Mock<IEntityPhoto> mockEntity = new Mock<IEntityPhoto>(MockBehavior.Loose);
            mockEntity.SetupGet(f => f.Photo).Returns(data);

            presenter.SetData(mockEntity.Object);

            mockViewInfo.Verify(f => f.SetPhoto(data), Times.Once);
        }

       
    }
}
