﻿using Contacts.Tests.Data;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using NUnit.Framework;
using System;

namespace Contacts.Tests.Viper.Data
{
    [TestFixture]
    public class TestDataWrapper
    {
        [Test]
        public void DataWrapperConstructor_Test()
        {
            DataList dataList = new DataList();
            Assert.DoesNotThrow(()=>
            {
                IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            });
        }

        [Test]
        public void DataWrapperConstructor_Null()
        {
            DataList dataList = new DataList();
            Assert.Throws<ArgumentNullException>(() =>
            {
                IDataWrapper dataWrapper = new DataWrapper(null);
            });
        }

        [Test]
        public void GetCount_Test()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);
            Assert.AreEqual(5, dataWrapper.GetCount(), "Count calculated incorrect");
        }

        [Test]
        public void GetData_ExistingItem()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);

            int id = 2;
            IEntityContact expected = new RepoContact(2, "Qui-Gon Jinn", "+380633213210", "https://image.ibb.co/drkFoy/Qui_Gon_min.jpg", "https://image.ibb.co/b3V4gJ/QuiGon.jpg");

            Assert.AreEqual(expected, dataWrapper.GetDataById(id), "Data taken incorrectly");
        }

        [Test]
        public void GetData_NonExistingItem()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);

            int id = 514;

            Assert.IsNull(dataWrapper.GetDataById(id), "Data doesn't exist, but was taken");
        }

        [Test]
        public void GetData_NegativeId()
        {
            DataList dataList = new DataList();
            IDataWrapper dataWrapper = new DataWrapper(dataList.Contacts);

            int id = -514;

            Assert.Throws<ArgumentException>(() =>
            {
                dataWrapper.GetDataById(id);
            });
        }
    }
}
