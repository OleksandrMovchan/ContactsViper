﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Repo.SQLiteHelper
{
    public interface ISQLite
    {
        string GetDatabasePath(string filename);
    }
}
