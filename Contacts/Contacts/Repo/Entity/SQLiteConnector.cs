﻿namespace Contacts.Repo.Entity
{
    public class SQLiteConnector
    {
        public const string DATABASE_NAME = "contacts.db";
        private ContactRepository _database;

        public ContactRepository Database
        {
            get
            {
                if (_database == null)
                {
                    _database = new ContactRepository(DATABASE_NAME);
                }
                return _database;
            }
        }
    }
}
