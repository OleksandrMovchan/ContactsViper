﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Repo.Entity.Interfaces
{
    public interface IContact
    {
        int Id { get; }
        string Name { get; set; }
        string Number { get; set; }
        string PhotoMin { get; set; }
        string PhotoFull { get; set; }
    }
}
