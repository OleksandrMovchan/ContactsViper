﻿using Contacts.Repo.Entity.Interfaces;
using Contacts.Repo.SQLiteHelper;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Contacts.Repo.Entity
{
    public class ContactRepository
    {
        SQLiteConnection database;

        public ContactRepository(string filename)
        {
            var a = DependencyService.Get<ISQLite>();
            string databasePath = a.GetDatabasePath(filename);
            database = new SQLiteConnection(databasePath);
            database.CreateTable<Contact>();
        }

        public List<Contact> GetItems()
        {
            return (from i in database.Table<Contact>() select i).ToList();
        }

        public Contact GetItem(int id)
        {
            return database.Get<Contact>(id);
        }

        public int DeleteItem(int id)
        {
            return database.Delete<Contact>(id);
        }

        public int SaveItem(Contact item)
        {
            if (item.Id != 0)
            {
                database.Update(item);
                return item.Id;
            }
            else
            {
                return database.Insert(item);
            }
        }
    }
}
