﻿using Contacts.Repo.Entity.Interfaces;
using SQLite;

namespace Contacts.Repo.Entity
{
    [Table("Contacts")]
    public class Contact : IContact
    {
        private int _id;
        private string _name;
        private string _number;
        private string _photoMin;
        private string _photoFull;

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get => _id; set => _id = value; }

        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public string PhotoMin { get => _photoMin; set => _photoMin = value; }
        public string PhotoFull { get => _photoFull; set => _photoFull = value; }
    }
}
