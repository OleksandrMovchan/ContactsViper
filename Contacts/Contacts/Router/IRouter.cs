﻿namespace Contacts.Router
{
    public interface IRouter
    {
        void GoToContactInfo(int id);
        void GoToPhoto(int id);
        void GoToEdit(int id);
        void GoBack();
    }
}