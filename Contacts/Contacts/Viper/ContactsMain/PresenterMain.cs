﻿using Contacts.Router;
using Contacts.Viper.ContactsMain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsMain
{
    public class PresenterMain : IPresenterMain
    {
        private IRouter _router;
        private IViewMain _view;

        public event Action OnBtnAddClicked;

        public PresenterMain(IRouter router, IViewMain view)
        {
            _router = router ?? throw new ArgumentNullException("router should not be null");
            _view = view ?? throw new ArgumentNullException("main view should not be null");

            _view.OnBtnAddClicked += AddClicked;
        }

        public void AddClicked()
        {
            Console.WriteLine("Presenter catched");
            OnBtnAddClicked?.Invoke();
        }

        public void GoToEdit(int id)
        {
            Console.WriteLine("Presenter finish catched");
            _router.GoToEdit(id);
        }
    }
}
