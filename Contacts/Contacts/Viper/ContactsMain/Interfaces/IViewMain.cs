﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsMain.Interfaces
{
    public interface IViewMain
    {
        event Action OnBtnAddClicked;
    }
}
