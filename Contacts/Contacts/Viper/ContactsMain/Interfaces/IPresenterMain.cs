﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsMain.Interfaces
{
    public interface IPresenterMain
    {
        event Action OnBtnAddClicked;
        
        void AddClicked();
        void GoToEdit(int id);
    }
}
