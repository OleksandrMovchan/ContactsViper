﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsMain.Interfaces
{
    public interface IInteractorMain
    {
        IPresenterMain Presenter { get; set; }

        void AddClicked();
    }
}
