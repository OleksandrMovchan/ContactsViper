﻿using Contacts.Viper.ContactsMain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsMain
{
    public class InteractorMain : IInteractorMain
    {
        private IPresenterMain _presenter;

        public IPresenterMain Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter != null)
                {
                    _presenter.OnBtnAddClicked -= AddClicked;

                }

                _presenter = value ?? throw new ArgumentNullException();

                _presenter.OnBtnAddClicked += AddClicked;
            }
        }

        public void AddClicked()
        {
            Console.WriteLine("Interactor catched");
            _presenter.GoToEdit(-1);
        }
    }
}
