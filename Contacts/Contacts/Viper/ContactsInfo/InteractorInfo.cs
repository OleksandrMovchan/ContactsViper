﻿using System;
using System.Collections.Generic;

using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Viper.ContactsInfo
{
    public class InteractorInfo : IInteractorInfo
    {
        private IDataWrapper _data;
        private IPresenterInfo _presenter;
        private int _id;

        /// <summary>
        /// Takes a instance of presenter and throws an exeption if null
        /// </summary>
        public IPresenterInfo Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter != null)
                {
                    _presenter.OnEditPress -= EditClicked;
                    _presenter.OnDeletePress -= DeleteClicked;

                }

                _presenter = value ?? throw new ArgumentNullException();

                _presenter.OnEditPress += EditClicked;
                _presenter.OnDeletePress += DeleteClicked;
            }
        }

        /// <summary>
        /// Create an instance of interactor
        /// </summary>
        /// <param name="data">IDataWrapper - takes a wrapped data and throws an exception if null</param>
        public InteractorInfo(IDataWrapper data)
        {
            _data = data ?? throw new ArgumentNullException("data should not be null");
        }

        /// <summary>
        /// Calls datawrapper's method to search data
        /// Calls data converter
        /// </summary>
        /// <returns>Retuurns converted entity, prepared for sending to presenter</returns>
        private IEntityInfo GetData()
        {
            IEntityContact data = _data.GetDataById(_id);
            return ConvertData(data);
        }

        /// <summary>
        /// Convert IEntityContact to IEntityInfo
        /// </summary>
        /// <param name="contact">IEntityContact - takes contact from repository </param>
        /// <returns>IEntityInfo - entity, prepared for presenter</returns>
        private IEntityInfo ConvertData(IEntityContact contact)
        {
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IEntityInfo user = new EntityInfo(name, number, photo);
            return user;
        }

        /// <summary>
        /// Method that set id of person in interactor
        /// Search necessary entity and send it to presentrer
        /// </summary>
        /// <param name="id">int - takes id of person</param>
        public void SetId(int id)
        {
            _id = id;
            IEntityInfo contact = GetData();
            _presenter.SetData(contact);
        }

        public void EditClicked()
        {
            Console.WriteLine("Interactor catched");
            _presenter.GoToEdit(_id);
        }

        public void DeleteClicked()
        {
            _data.DeleteData(_id);
            _presenter.BackClicked();
        }
    }
}