﻿using Contacts.Router;
using Contacts.Viper.ContactsInfo.Interfaces;
using System;

namespace Contacts.Viper.ContactsInfo
{
    public class PresenterInfo : IPresenterInfo
    {
        public event Action OnEditPress;
        public event Action OnDeletePress;

        private IViewInfo _view;
        private IRouter _router;

        /// <summary>
        /// Create an instance of presenter
        /// </summary>
        /// <param name="view">IViewInfo - takes a view instance and throws an exception if null</param>
        /// <param name="router">IRouter - takes a router instance and throws an exception if null</param>
        public PresenterInfo(IViewInfo view, IRouter router)
        {
            _view = view ?? throw new ArgumentNullException("view should not be null");
            _router = router ?? throw new ArgumentNullException("router should not be null");

            _view.OnEditClick += EditClicked;
            _view.OnDeleteClick += DeleteClicked;
        }

        /// <summary>
        /// If back button pressed - calls routers method to return in previous screen 
        /// </summary>
        public void BackClicked()
        {
            _router.GoBack();
        }

        public void DeleteClicked()
        {
            OnEditPress?.Invoke();
        }

        public void EditClicked()
        {
            Console.WriteLine("Presenter catched");
            OnDeletePress?.Invoke();
        }

        /// <summary>
        /// Takes an entity, prepared by interactor and set fields in view
        /// </summary>
        /// <param name="contact">IEntityInfo - entity, prepared by interactor</param>
        public void SetData(IEntityInfo contact)
        {
            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }

        public void GoToEdit(int id)
        {
            Console.WriteLine("Presenter catched 2nd time");
            _router.GoToEdit(id);
        }
    }
}