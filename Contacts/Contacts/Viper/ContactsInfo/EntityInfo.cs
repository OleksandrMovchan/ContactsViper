﻿using Contacts.Viper.ContactsInfo.Interfaces;
using System.Collections.Generic;

namespace Contacts.Viper.ContactsInfo
{
    public class EntityInfo : IEntityInfo
    {
        string _name;
        string _number;
        string _photo;

        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public string Photo { get => _photo; set => _photo = value; }

        /// <summary>
        /// Create an instance of entity object to show in detail info
        /// </summary>
        /// <param name="name">string - Takes a name of person</param>
        /// <param name="number">string - Takes a phone number of person</param>
        /// <param name="photo">string - Takes a url with minimized photo</param>
        public EntityInfo(string name, string number, string photo)
        {
            _name = name;
            _number = number;
            _photo = photo;
        }

        public override bool Equals(object obj)
        {
            var info = obj as EntityInfo;
            return info != null &&
                   _name == info._name &&
                   _number == info._number &&
                   _photo == info._photo;
        }

        public override int GetHashCode()
        {
            var hashCode = 1590447302;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_number);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_photo);
            return hashCode;
        }
    }
}