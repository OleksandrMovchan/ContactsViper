﻿using Contacts.Router;
using System;

namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IPresenterInfo
    {
        event Action OnEditPress;
        event Action OnDeletePress;

        void BackClicked();
        void SetData(IEntityInfo contact);
        void EditClicked();
        void DeleteClicked();
        void GoToEdit(int id);
    }
}