﻿using System;

namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IViewInfo
    {
        event Action OnEditClick;
        event Action OnDeleteClick;

        void SetName(string name);
        void SetNumber(string number);
        void SetPhoto(string photo);
    }
}