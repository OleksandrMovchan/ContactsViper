﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IEntityInfo
    {
        string Name { get; }
        string Number { get; }
        string Photo { get; }
    }
}