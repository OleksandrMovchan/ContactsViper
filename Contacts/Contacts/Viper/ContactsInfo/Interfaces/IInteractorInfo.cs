﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IInteractorInfo
    {
        IPresenterInfo Presenter { get; set; }

        void EditClicked();
        void DeleteClicked();
        void SetId(int id);
    }
}