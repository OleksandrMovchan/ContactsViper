﻿namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IViewPhoto
    {
        void SetPhoto(string photo);
    }
}