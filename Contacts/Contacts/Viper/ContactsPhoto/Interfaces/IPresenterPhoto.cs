﻿using Contacts.Router;

namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IPresenterPhoto
    {
        void BackClicked();
        void SetData(IEntityPhoto photo);
    }
}