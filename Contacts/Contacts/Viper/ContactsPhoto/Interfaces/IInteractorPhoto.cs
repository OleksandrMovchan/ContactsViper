﻿namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IInteractorPhoto
    {
        IPresenterPhoto Presenter { get; set; }
        void SetId(int id);
    }
}