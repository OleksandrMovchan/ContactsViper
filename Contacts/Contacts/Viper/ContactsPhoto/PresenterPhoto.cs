﻿using System;
using Contacts.Router;
using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Viper.ContactsPhoto
{
    public class PresenterPhoto : IPresenterPhoto
    {
        private IViewPhoto _view;
        private IRouter _router;

        public PresenterPhoto(IViewPhoto view, IRouter router)
        {
            _view = view ?? throw new ArgumentNullException("view should not be null");
            _router = router ?? throw new ArgumentNullException("view should not be null");
        }

        public void BackClicked()
        {
            _router.GoBack();
        }

        public void SetData(IEntityPhoto photo)
        {
            _view.SetPhoto(photo.Photo);
        }
    }
}