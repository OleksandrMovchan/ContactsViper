﻿using Contacts.Viper.ContactsPhoto.Interfaces;
using System.Collections.Generic;

namespace Contacts.Viper.ContactsPhoto
{
    public class EntityPhoto : IEntityPhoto
    {
        int _id;
        string _photo;

        public int Id { get => _id; set => _id = value; }
        public string Photo { get => _photo; set => _photo = value; }

        public EntityPhoto(int id, string photo)
        {
            _id = id;
            _photo = photo;
        }

        public override bool Equals(object obj)
        {
            var photo = obj as EntityPhoto;
            return photo != null &&
                   _id == photo._id &&
                   _photo == photo._photo;
        }

        public override int GetHashCode()
        {
            var hashCode = -1585980767;
            hashCode = hashCode * -1521134295 + _id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_photo);
            return hashCode;
        }
    }
}