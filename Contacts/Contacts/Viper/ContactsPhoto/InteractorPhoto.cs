﻿using System;
using System.Collections.Generic;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Viper.ContactsPhoto
{
    public class InteractorPhoto : IInteractorPhoto
    {
        private IDataWrapper _data;
        private IPresenterPhoto _presenter;
        private int _id;

        public IPresenterPhoto Presenter { get => _presenter;
            set => _presenter = value ?? throw new ArgumentNullException("Presenter should not be null"); }

        public InteractorPhoto(IDataWrapper data)
        {
            _data = data ?? throw new ArgumentNullException("view should not be null");
        }

        private IEntityPhoto GetData()
        {
            IEntityContact data = _data.GetDataById(_id);
            return ConvertData(data);
        }

        private IEntityPhoto ConvertData(IEntityContact contact)
        {
            int id = contact.Id;
            string photo = contact.PhotoFull;

            IEntityPhoto user = new EntityPhoto(id, photo);
            return user;
        }

        public void SetId(int id)
        {
            _id = id;
            IEntityPhoto photo = GetData();
            _presenter.SetData(photo);
            
        }
    }
}