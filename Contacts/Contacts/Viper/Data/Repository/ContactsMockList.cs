﻿using System.Collections.Generic;

using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.Data.Repository
{
    public class ContactsMockList
    {
        public List<IEntityContact> Contacts { get; set; }

        public ContactsMockList()
        {
            FillList();
        }

        private void FillList()
        {
            Contacts = new List<IEntityContact>
            {
                new RepoContact(0, /*Resource.Drawable.Yoda_min,*/ "Master Yoda", "+380633213210", "https://image.ibb.co/nP81Ty/Yoda_min.jpg", "https://image.ibb.co/efDqMJ/Yoda.jpg"),
                new RepoContact(1, /*Resource.Drawable.ObiWan_min,*/ "Obi-Wan Kenobi", "+380633213210", "https://image.ibb.co/e2ftad/Obi_Wan_min.jpg", "https://image.ibb.co/crGo8y/ObiWan.jpg"),
                new RepoContact(2, /*Resource.Drawable.QuiGon_min,*/ "Qui-Gon Jinn", "+380633213210", "https://image.ibb.co/drkFoy/Qui_Gon_min.jpg", "https://image.ibb.co/b3V4gJ/QuiGon.jpg"),
                new RepoContact(3, /*Resource.Drawable.MaceWindu_min,*/ "Mace Windu", "+380633213210", "https://image.ibb.co/mAuKFd/Mace_Windu_min.jpg", "https://image.ibb.co/ezagTy/Mace_Windu.jpg"),
                new RepoContact(4, /*Resource.Drawable.Luke_min,*/ "Luke Skywalker", "+380633213210", "https://image.ibb.co/ex26vd/Luke_min.jpg", "https://image.ibb.co/nJjmvd/Luke.jpg"),
                new RepoContact(5, /*Resource.Drawable.KiAdi_min,*/ "Ki Adi Mundi", "+380633213210", "https://image.ibb.co/iS1AMJ/Ki_Adi_min.jpg", "https://image.ibb.co/eMteFd/KiAdi.jpg"),
                new RepoContact(6, /*Resource.Drawable.Maul_min,*/ "Darth Maul", "+380633213210", "https://image.ibb.co/gxd1Ty/Maul_min.jpg", "https://image.ibb.co/mgMMTy/Maul.jpg"),
                new RepoContact(7, /*Resource.Drawable.Dooku_min,*/ "Count Dooku", "+380633213210", "https://image.ibb.co/iFzx1J/Dooku_min.jpg", "https://image.ibb.co/gRBAMJ/Dooku.jpg"),
                new RepoContact(8, /*Resource.Drawable.Palpatine_min,*/ "Emperor Palpatine", "+380633213210", "https://image.ibb.co/ivgMTy/Palpatine_min.jpg", "https://image.ibb.co/h9Atad/Palpatine.jpg"),
                new RepoContact(9, /*Resource.Drawable.Vader_min,*/ "Darth Vader", "+380633213210", "https://image.ibb.co/kp2T8y/Vader_min.jpg", "https://image.ibb.co/dsbAMJ/Vader.jpg"),
                new RepoContact(10, /*Resource.Drawable.Krennik_min,*/ "Orson Krennik", "+380633213210", "https://image.ibb.co/npBo8y/Krennik_min.jpg", "https://image.ibb.co/k2wc1J/Krennik.jpg"),
                new RepoContact(11, /*Resource.Drawable.Tarkin_min,*/ "Grand Moff Tarkin", "+380633213210", "https://image.ibb.co/btYqMJ/Tarkin_min.jpg", "https://image.ibb.co/fuHH1J/Tarkin.jpg"),
                new RepoContact(12, /*Resource.Drawable.Leia_min,*/ "Leia Organa", "+380633213210", "https://image.ibb.co/b5tPgJ/Leia_min.jpg", "https://image.ibb.co/dFHT8y/Leia.jpg"),
                new RepoContact(13, /*Resource.Drawable.Solo_min,*/ "Han Solo", "+380633213210", "https://image.ibb.co/fOqFoy/Solo_min.jpg", "https://image.ibb.co/jUzmvd/Solo.jpg"),
                new RepoContact(14, /*Resource.Drawable.Chewbacca_min,*/ "Chewbacca", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(15, /*Resource.Drawable.JarJar_min,*/ "Jar Jar Binks", "+380633213210", "https://image.ibb.co/jMxpFd/Jar_Jar_min.jpg", "https://image.ibb.co/gbQbvd/JarJar.jpg"),
                new RepoContact(16, /*Resource.Drawable.Jabba_min,*/ "Jabba Hutt", "+380633213210", "https://image.ibb.co/j4yJ8y/Jabba_min.jpg", "https://image.ibb.co/jvikoy/Jabba.jpg"),
                new RepoContact(17, /*Resource.Drawable.Rey_min,*/ "Rey", "+380633213210", "https://image.ibb.co/hsGzFd/Rey_min.jpg", "https://image.ibb.co/nsQFoy/Rey.jpg"),
                new RepoContact(18, /*Resource.Drawable.Finn_min,*/ "Finn", "+380633213210", "https://image.ibb.co/nFx6vd/Finn_min.jpg", "https://image.ibb.co/i16c1J/Finn.jpg"),
                new RepoContact(19, /*Resource.Drawable.Poe_min,*/ "Poe Dameron", "+380633213210", "https://image.ibb.co/cK76vd/Poe_min.jpg", "https://image.ibb.co/fOXvoy/Poe.jpg"),
                new RepoContact(20, /*Resource.Drawable.Hux_min,*/ "General Hux", "+380633213210", "https://image.ibb.co/cxGS1J/Hux_min.jpg", "https://image.ibb.co/mENOad/Hux.jpg"),
                new RepoContact(21, /*Resource.Drawable.KyloRen_min,*/ "Kylo Ren", "+380633213210", "https://image.ibb.co/hqZ88y/Kylo_Ren_min.jpg", "https://image.ibb.co/mSZVMJ/KyloRen.jpg"),
                new RepoContact(22, /*Resource.Drawable.Phasma_min,*/ "Captain Phasma", "+380633213210", "https://image.ibb.co/gHmMTy/Phasma_min.jpg", "https://image.ibb.co/iKST8y/Phasma.jpg"),
                new RepoContact(23, /*Resource.Drawable.DJ_min,*/ "DJ", "+380633213210", "https://image.ibb.co/kuktad/DJ_min.jpg", "https://image.ibb.co/hf4VMJ/DJ.jpg"),
                new RepoContact(24, /*Resource.Drawable.Snoke_min,*/ "Supreme Leader Snoke", "+380633213210", "https://image.ibb.co/bN1AMJ/Snoke_min.jpg", "https://image.ibb.co/n2go8y/Snoke.jpg")

            };
        }
    }
}