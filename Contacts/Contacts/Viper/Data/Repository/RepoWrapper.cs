﻿using Contacts.Repo.Entity;
using Contacts.Repo.Entity.Interfaces;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.Data.Repository
{
    public class RepoWrapper : IDataWrapper
    {
        private SQLiteConnector _connector;

        public RepoWrapper()
        {
            _connector = new SQLiteConnector();
        }

        public int GetCount()
        {
            return _connector.Database.GetItems().Count;
        }

        public IEntityContact GetDataByPosition(int position)
        {
            List<Contact> contacts = _connector.Database.GetItems();
            IEntityContact repoContact = new RepoContact(contacts[position].Id, contacts[position].Name, contacts[position].Number, contacts[position].PhotoMin, contacts[position].PhotoFull);
            return repoContact;
        }

        public IEntityContact GetDataById(int id)
        {
            IContact contact = _connector.Database.GetItem(id);
            IEntityContact repoContact = new RepoContact(contact.Id, contact.Name, contact.Number, contact.PhotoMin, contact.PhotoFull);
            return repoContact;
        }

        public void AddData(string name, string number)
        {
            Contact contact = new Contact();
            contact.Name = name;
            contact.Number = number;
            contact.PhotoMin = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTG-kIngD9CVsoBGVq0qr2iMcZKpC4QM_0b6YqrxYrn9L5HDaGhqQ";
            contact.PhotoFull = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTG-kIngD9CVsoBGVq0qr2iMcZKpC4QM_0b6YqrxYrn9L5HDaGhqQ";
            _connector.Database.SaveItem(contact);
        }

        public void ChangeData(int id, string name, string number)
        {
            Contact contact = _connector.Database.GetItem(id);
            contact.Name = name;
            contact.Number = number;
            _connector.Database.SaveItem(contact);
        }

        public void DeleteData(int id)
        {
            _connector.Database.DeleteItem(id);
        }
    }
}
