﻿using Contacts.Repo.Entity;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using System;
using System.Collections.Generic;

namespace Contacts.Viper.Data.Repository
{
    public class DataWrapper : IDataWrapper
    {
        private List<IEntityContact> _data;

        public DataWrapper()
        {
            ContactsMockList list = new ContactsMockList();
            _data = list.Contacts;
        }

        public DataWrapper(List<IEntityContact> data)
        {
            _data = data ?? throw new ArgumentNullException("Data should not be null");
        }

        public SQLiteConnector Connector => throw new NotImplementedException();

        public void AddData(string name, string number)
        {
            throw new NotImplementedException();
        }

        public void ChangeData(int id, string name, string number)
        {
            throw new NotImplementedException();
        }

        public void DeleteData(int id)
        {
            throw new NotImplementedException();
        }

        public int GetCount()
        {
            return _data.Count;
        }

        public IEntityContact GetDataById(int id)
        {
            if (id < 0) throw new ArgumentException("Id should not have a negative value");
            foreach (IEntityContact contact in _data)
            {
                if (contact.Id == id)
                {
                    return contact;
                }
            }
            return null;
        }

        public IEntityContact GetDataByPosition(int id)
        {
            if (id < 0) throw new ArgumentException("Id should not have a negative value");
            foreach (IEntityContact contact in _data)
            {
                if (contact.Id == id)
                {
                    return contact;
                }
            }
            return null;
        }
    }
}
