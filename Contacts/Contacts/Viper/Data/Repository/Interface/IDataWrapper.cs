﻿using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.Data.Repository.Interface
{
    public interface IDataWrapper
    {
        IEntityContact GetDataByPosition(int id);
        IEntityContact GetDataById(int id);
        int GetCount();

        void AddData(string name, string number);
        void ChangeData(int id, string name, string number);
        void DeleteData(int id);
    }
}
