﻿using Contacts.Router;
using Contacts.Viper.ContactsEdit.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit
{
    public class PresenterEdit : IPresenterEdit
    {
        private IRouter _router;
        private IViewEdit _view;

        public event Action OnConfirmClicked;

        public PresenterEdit(IViewEdit view, IRouter router)
        {
            _view = view ?? throw new ArgumentNullException("view should not be null");
            _router = router ?? throw new ArgumentNullException("router should not be null");
            _view.OnConfirmClicked += ConfirmClicked;
        }

        public void ConfirmClicked()
        {
            OnConfirmClicked?.Invoke();
        }

        public void GoBack()
        {
            _router.GoBack();
        }

        public void SetData(IEntityEdit contact)
        {
            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
        }
    }
}
