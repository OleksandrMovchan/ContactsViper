﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit.Interfaces
{
    public interface IViewEdit
    {
        event Action OnConfirmClicked;

        void SetName(string name);
        void SetNumber(string number);
    }
}
