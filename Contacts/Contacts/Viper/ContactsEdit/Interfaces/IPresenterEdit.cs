﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit.Interfaces
{
    public interface IPresenterEdit
    {
        event Action OnConfirmClicked;

        void SetData(IEntityEdit contact);
        void ConfirmClicked();
        void GoBack();
    }
}
