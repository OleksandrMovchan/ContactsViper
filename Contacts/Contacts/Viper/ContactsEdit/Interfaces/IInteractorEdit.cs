﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit.Interfaces
{
    public interface IInteractorEdit
    {
        IPresenterEdit Presenter { get; set; }
        IEntityEdit Entity { get; set; }

        void ConfirmClicked();
        void SetId(int id);
    }
}
