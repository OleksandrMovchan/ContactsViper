﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit.Interfaces
{
    public interface IEntityEdit
    {
        int Id { get; set; }
        string Name { get; set; }
        string Number { get; set; }
    }
}
