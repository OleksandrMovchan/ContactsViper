﻿using Contacts.Viper.ContactsEdit.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit
{
    public class InteractorEdit : IInteractorEdit
    {
        private IDataWrapper _data;
        private IPresenterEdit _presenter;
        IEntityEdit _entity;
        private int _id = -1;

        public IPresenterEdit Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter != null)
                {
                    _presenter.OnConfirmClicked -= ConfirmClicked;

                }

                _presenter = value ?? throw new ArgumentNullException();

                _presenter.OnConfirmClicked += ConfirmClicked;
            }
        }

        public IEntityEdit Entity { get => _entity; set => _entity = value; }

        public InteractorEdit(IDataWrapper data)
        {
            _data = data ?? throw new ArgumentNullException("data should not be null");
        }

        public void ConfirmClicked()
        {
            if (_id >= 0)
            {
                _data.ChangeData(_entity.Id, _entity.Name, _entity.Number);
            }
            else
            {
                _data.AddData(_entity.Name, _entity.Number);
            }
            _presenter.GoBack();
        }

        public void SetId(int id)
        {
            _id = id;
            _presenter.SetData(GetData(_id));
        }

        private IEntityEdit GetData(int id)
        {
            IEntityContact data = _data.GetDataById(id);
            return ConvertData(data);
        }

        private IEntityEdit ConvertData(IEntityContact contact)
        {
            int id = contact.Id;
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IEntityEdit user = new EntityEdit();
            user.Name = name;
            user.Number = number;
            return user;
        }
    }
}
