﻿using Contacts.Viper.ContactsEdit.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsEdit
{
    public class EntityEdit : IEntityEdit
    {
        private int _id;
        private string _name;
        private string _number;

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }

        public override bool Equals(object obj)
        {
            var edit = obj as EntityEdit;
            return edit != null &&
                   _id == edit._id &&
                   _name == edit._name &&
                   _number == edit._number;
        }

        public override int GetHashCode()
        {
            var hashCode = -150498637;
            hashCode = hashCode * -1521134295 + _id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_number);
            return hashCode;
        }
    }


}
