﻿using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;
using System;

namespace Contacts.Viper.ContactsList
{
    public class PresenterContact : IPresenterContact
    {
        private IRouter _router;
        private IViewContact _view;

        public event Action OnImagePress;
        public event Action OnCardPress;

        /// <summary>
        /// Create an instance of presenter
        /// Subscribe events
        /// </summary>
        /// <param name="view">IViewContact - takes a view instance and throws an exception if null</param>
        /// <param name="router">IRouter - takes a router instance and throws an exception if null</param>
        public PresenterContact(IViewContact view, IRouter router)
        {
            _view = view ?? throw new ArgumentNullException("view should not be null");
            _router = router ?? throw new ArgumentNullException("router should not be null");
            _view.OnCardClick += CardClicked;
            _view.OnImageClick += AvatarClicked;
        }

        /// <summary>
        /// Invoke method when avatar clicked in Interactor
        /// </summary>
        public void AvatarClicked()
        {
            OnImagePress?.Invoke();
        }

        /// <summary>
        /// Invoke method when card clicked in Interactor
        /// </summary>
        public void CardClicked()
        {
            OnCardPress?.Invoke();
        }

        /// <summary>
        /// Calls router's method GoToContactInfo and transfer id from Interactor after verification
        /// </summary>
        /// <param name="id">int - param id transfered from interactor</param>
        public void GoToContactInfo(int id)
        {
            _router.GoToContactInfo(id);
        }

        /// <summary>
        /// Calls router's method GoToPhoto and transfer id from Interactor after verification
        /// </summary>
        /// <param name="id">int - param id transfered from interactor</param>
        public void GoToPhoto(int id)
        {
            _router.GoToPhoto(id);
        }

        /// <summary>
        /// Takes an entity, prepared by interactor and set fields in view
        /// </summary>
        /// <param name="contact">IUserEntityContact - entity, prepared by interactor</param>
        public void SetData(IUserEntityContact contact)
        {
            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }
    }
}