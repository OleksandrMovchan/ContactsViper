﻿using Contacts.Viper.ContactsList.Enums;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using System;

namespace Contacts.Viper.ContactsList
{
    public class InteractorContact : IInteractorContact
    {
        private IDataWrapper _data;
        private IPresenterContact _presenter;
        private int _position;
        private int _id;

        /// <summary>
        /// Presenter property
        /// in Set throws exception if null
        /// resubscribe in events
        /// </summary>
        public IPresenterContact Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter != null)
                {
                    _presenter.OnCardPress -= CardClicked;
                    _presenter.OnImagePress -= AvatarClicked;

                }

                _presenter = value ?? throw new ArgumentNullException();

                _presenter.OnCardPress += CardClicked;
                _presenter.OnImagePress += AvatarClicked;
            }
        }

        /// <summary>
        /// Create an instance of interactor
        /// </summary>
        /// <param name="data">IDataWrapper - takes a wrapped data and throws an exception if null</param>
        public InteractorContact(IDataWrapper data)
        {
            _data = data ?? throw new ArgumentNullException();
        }

        /// <summary>
        /// Calls datawrapper's method to search data
        /// Calls data converter
        /// </summary>
        /// <param name="id">int - takes id of contact to find data</param>
        /// <returns>Retuurns converted entity, prepared for sending to presenter</returns>
        private IUserEntityContact GetData(int id)
        {
            IEntityContact data = _data.GetDataByPosition(id);
            return ConvertData(data);
        }

        /// <summary>
        /// Convert IEntityContact to IUserEntityContact
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>IUserEntityContact - entity, prepared for presenter</returns>
        private IUserEntityContact ConvertData(IEntityContact contact)
        {
            int id = contact.Id;
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IUserEntityContact user = new UserContact(id, name, number, photo);
            return user;
        }

        /// <summary>
        /// In avatar was clicked verify, is it possible to do transition on next screen
        /// </summary>
        public void AvatarClicked()
        {
            VerificationResult result;
            // DO SOMETHING
            result = VerificationResult.ALLOWED;

            if (result == VerificationResult.ALLOWED)
            {
                _presenter.GoToPhoto(_id);
            }
        }

        /// <summary>
        /// In card was clicked verify, is it possible to do transition on next screen
        /// </summary>
        public void CardClicked()
        {
            // DO SOMETHING
            VerificationResult result;
            // DO SOMETHING
            result = VerificationResult.ALLOWED;

            if (result == VerificationResult.ALLOWED)
            {
                _presenter.GoToContactInfo(_id);
            }
        }

        /// <summary>
        /// Method that set id of person in interactor
        /// Search necessary entity and send it to presentrer
        /// </summary>
        /// <param name="id">int - takes id of person</param>
        public void SetId(int position)
        {
            _position = position;
            IUserEntityContact contact = GetData(_position);
            _id = contact.Id;
            _presenter.SetData(contact);
        }
    }
}