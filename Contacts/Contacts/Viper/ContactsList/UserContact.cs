﻿using Contacts.Viper.ContactsList.Interfaces;
using System.Collections.Generic;

namespace Contacts.Viper.ContactsList
{
    public class UserContact : IUserEntityContact
    {
        private int _id;
        private string _name;
        private string _number;
        private string _photo;

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public string Photo { get => _photo; set => _photo = value; }

        /// <summary>
        /// Create an instance of contact to show in contact card
        /// </summary>
        /// <param name="id">int - id primary key of contact</param>
        /// <param name="name">string - name of person</param>
        /// <param name="number">string - number of person</param>
        /// <param name="photo">string - url with minimized photo</param>
        public UserContact(int id,  string name, string number, string photo)
        {
            _id = id;
            _name = name;
            _number = number;
            _photo = photo;
        }

        public override bool Equals(object obj)
        {
            var contact = obj as UserContact;
            return contact != null &&
                   Id == contact.Id &&
                   Name == contact.Name &&
                   Number == contact.Number &&
                   Photo == contact.Photo;
        }

        public override int GetHashCode()
        {
            var hashCode = -2131899991;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Number);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Photo);
            return hashCode;
        }
    }
}