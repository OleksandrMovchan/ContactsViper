﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.ContactsList.Enums
{
    public enum VerificationResult
    {
        ALLOWED, DENIED 
    }
}
