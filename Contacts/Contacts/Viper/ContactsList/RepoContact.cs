﻿using Contacts.Viper.ContactsList.Interfaces;
using System.Collections.Generic;

namespace Contacts.Viper.ContactsList
{
    public class RepoContact : IEntityContact
    {
        private string _name;
        private string _number;
        private string _photoMin;
        private string _photoFull;
        private int _id;

        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public int Id { get => _id; set => _id = value; }
        public string PhotoMin { get => _photoMin; set => _photoMin = value; }
        public string PhotoFull { get => _photoFull; set => _photoFull = value; }

        /// <summary>
        /// Create an instance of contact from database
        /// </summary>
        /// <param name="id">int - id primary key of contact</param>
        /// <param name="name">string - name of person</param>
        /// <param name="number">string - number of person</param>
        /// <param name="photoMin">string - url with minimized photo</param>
        /// <param name="photoFull">string - url with large photo</param>
        public RepoContact(int id, string name, string number, string photoMin, string photoFull)
        {
            _id = id;
            _name = name;
            _number = number;
            _photoMin = photoMin;
            _photoFull = photoFull;
        }

        public override bool Equals(object obj)
        {
            var contact = obj as RepoContact;
            return contact != null &&
                   _name == contact._name &&
                   _number == contact._number &&
                   _photoMin == contact._photoMin &&
                   _photoFull == contact._photoFull &&
                   _id == contact._id;
        }

        public override int GetHashCode()
        {
            var hashCode = 1064124828;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_number);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_photoMin);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_photoFull);
            hashCode = hashCode * -1521134295 + _id.GetHashCode();
            return hashCode;
        }
    }
}