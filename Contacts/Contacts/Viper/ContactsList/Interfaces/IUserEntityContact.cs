﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IUserEntityContact
    {
        int Id { get; }
        string Name { get; }
        string Number { get; }
        string Photo { get; }
    }
}