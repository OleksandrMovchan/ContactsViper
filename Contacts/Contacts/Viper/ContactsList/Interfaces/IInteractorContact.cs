﻿using Contacts.Router;
using Contacts.Viper.ContactsList.Enums;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IInteractorContact
    {
        IPresenterContact Presenter { get; set; }
        
        void AvatarClicked();
        void CardClicked();
        void SetId(int id);

    }
}