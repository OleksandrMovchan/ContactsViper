﻿using Contacts.Router;
using System;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IPresenterContact
    {
        event Action OnCardPress;
        event Action OnImagePress;

        void SetData(IUserEntityContact contact);
        void AvatarClicked();
        void CardClicked();
        void GoToContactInfo(int id);
        void GoToPhoto(int id);
    }
}