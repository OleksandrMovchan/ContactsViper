﻿using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Activities
{
    [Activity(Label = "ContactDetailActivity")]
    public class ContactDetailActivity : Activity
    {
        public const string IdKey = "idKey";
        private IInteractorInfo _interactor;
        private int _id;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DetailScreen);

            //filling data collection
            IDataWrapper data = new RepoWrapper();

            //filling userinfo
            _id = Intent.GetIntExtra(IdKey, -1);
            var infoView = FindViewById<InfoView>(Resource.Id.infoview);

            IRouter router = new AndroidRouter(this);

            IPresenterInfo presenter = new PresenterInfo(infoView, router);

            _interactor = new InteractorInfo(data);
            _interactor.Presenter = presenter;

            _interactor.SetId(_id);

            //button clict listener
            var btnBack = FindViewById<Button>(Resource.Id.info_btnback);
            var btnEdit = FindViewById<Button>(Resource.Id.info_btnback);
            var btnDelete = FindViewById<Button>(Resource.Id.info_btnback);

            btnBack.Click += delegate
            {
                presenter.BackClicked();
            };
        }

        protected override void OnResume()
        {
            base.OnResume();
            _interactor.SetId(_id);
        }
    }
}