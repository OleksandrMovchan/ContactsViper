﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Activities
{
    [Activity(Label = "PhotoActivity")]
    public class PhotoActivity : Activity
    {
        

        public const string IdKey = "idKey";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.PhotoActivity);

            //filling data collection
            IDataWrapper data = new RepoWrapper();

            //filling userinfo
            int id = Intent.GetIntExtra(IdKey, -1);
            var photocardView = FindViewById<PhotocardView>(Resource.Id.photocard_view);

            IRouter router = new AndroidRouter(this);
            IPresenterPhoto presenter = new PresenterPhoto(photocardView, router);

            IInteractorPhoto interactor = new InteractorPhoto(data);
            interactor.Presenter = presenter;
            interactor.SetId(id);

            //button clict listener
            var btnBack = FindViewById<Button>(Resource.Id.btnphoto_back);
            btnBack.Click += delegate
            {
                presenter.BackClicked();
            };
        }
    }
}