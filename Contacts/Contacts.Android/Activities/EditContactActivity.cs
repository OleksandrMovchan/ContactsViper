﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsEdit;
using Contacts.Viper.ContactsEdit.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Activities
{
    [Activity(Label = "EditContactView")]
    public class EditContactActivity : Activity, IViewEdit
    {
        public const string IdKey = "idKey";
        public event Action OnConfirmClicked;

        private IInteractorEdit _interactor;
        private int _id;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EditContactActivity);

            //filling data collection
            IDataWrapper data = new RepoWrapper();

            //filling userinfo
            _id = Intent.GetIntExtra(IdKey, -1);

            IRouter router = new AndroidRouter(this);

            IPresenterEdit presenter = new PresenterEdit(this, router);

            _interactor = new InteractorEdit(data);
            _interactor.Presenter = presenter;

            if (_id > 0)
            {
                _interactor.SetId(_id);
            }
            

            //button clict listener
            var btnBack = FindViewById<Button>(Resource.Id.edit_btnBack);
            var btnConfirn = FindViewById<Button>(Resource.Id.edit_btnConfirm);

            btnBack.Click += delegate
            {
                presenter.GoBack();
            };

            btnConfirn.Click += Confirm_Click;
        }

        private void Confirm_Click(object sender, EventArgs e)
        {
            var editName = FindViewById<EditText>(Resource.Id.edit_name);
            var editNumber = FindViewById<EditText>(Resource.Id.edit_number);

            IEntityEdit entity = new EntityEdit();
            entity.Name = editName.Text.ToString();
            entity.Number = editNumber.Text.ToString();
            if (_id > 0)
            {
                entity.Id = _id;
            }

            _interactor.Entity = entity; 
            OnConfirmClicked?.Invoke();
        }

        public void SetName(string name)
        {
            var editName = FindViewById<EditText>(Resource.Id.edit_name);
            editName.Text = name;
        }

        public void SetNumber(string number)
        {
            var editNumber = FindViewById<EditText>(Resource.Id.edit_number);
            editNumber.Text = number;
        }

    }


}