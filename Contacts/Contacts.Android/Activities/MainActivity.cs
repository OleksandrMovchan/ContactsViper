﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using Contacts.Droid.Adapter;
using Contacts.Droid.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsMain;
using Contacts.Viper.ContactsMain.Interfaces;
using System;

namespace Contacts.Droid
{
    [Activity(Label = "Contacts", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, IViewMain
    {
        public event Action OnBtnAddClicked;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Forms.Forms.Init(this, savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            
            InitializeViper();

            var btnAdd = FindViewById<Button>(Resource.Id.main_btnAdd);

            if (!btnAdd.HasOnClickListeners)
            {
                btnAdd.Click += BtnAdd_Click;
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            InitializeRecycler();
        }

        private void InitializeRecycler()
        {
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.contacts_list);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);

            ContactsAdapter contactsAdapter = new ContactsAdapter();
            recyclerView.SetAdapter(contactsAdapter);
        }

        private void InitializeViper()
        {
            IRouter router = new AndroidRouter(this);

            IPresenterMain presenter = new PresenterMain(router, this);

            IInteractorMain interactor = new InteractorMain();
            interactor.Presenter = presenter;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Console.WriteLine("1Main catched");
            OnBtnAddClicked?.Invoke();
        }
    }
}

