package md573014c3dd55ac944b95bfadd14727b74;


public class ContactsViewHolderLeft
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Contacts.Droid.Adapter.ContactsViewHolderLeft, Contacts.Android", ContactsViewHolderLeft.class, __md_methods);
	}


	public ContactsViewHolderLeft (android.view.View p0)
	{
		super (p0);
		if (getClass () == ContactsViewHolderLeft.class)
			mono.android.TypeManager.Activate ("Contacts.Droid.Adapter.ContactsViewHolderLeft, Contacts.Android", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
