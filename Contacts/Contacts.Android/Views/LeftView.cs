﻿using System;
using System.Threading.Tasks;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.LeftView")]
    class LeftView : LinearLayout, IViewContact
    {
        private LayoutInflater _inflater;
        //IPresenterContact _presenter;

        //public IPresenterContact Presenter { get => _presenter; set => _presenter = value; }

        public event Action OnImageClick;
        public event Action OnCardClick;

        public LeftView(Context context) : base(context)
        {
        }

        public LeftView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public LeftView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public LeftView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected LeftView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.LeftView, this, true);
            var card = FindViewById<CardView>(Resource.Id.lv_card);
            var photo = FindViewById<ImageView>(Resource.Id.lv_photo);

            card.Radius = 72;

            if (!photo.HasOnClickListeners)
            {
                photo.Click += Image_Click;
            }

            if (!card.HasOnClickListeners)
            {
                card.Click += Card_Click;
            }

        }

        private void Card_Click(object sender, EventArgs e)
        {
            OnCardClick?.Invoke();
        }

        private void Image_Click(object sender, EventArgs e)
        {            
            OnImageClick?.Invoke();
        }

        public void SetName(string name)
        {
            var nameField = FindViewById<TextView>(Resource.Id.lv_name);
            nameField.Text = name;
        }

        public void SetNumber(string number)
        {
            var numberField = FindViewById<TextView>(Resource.Id.lv_number);
            numberField.Text = number;
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.lv_photo);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }
    }
}