﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.PhotocardView")]
    class PhotocardView : LinearLayout, IViewPhoto
    {
        private LayoutInflater _inflater;

        public PhotocardView(Context context) : base(context)
        {
        }

        public PhotocardView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public PhotocardView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public PhotocardView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected PhotocardView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.PhotocardView, this, true);
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.photo_img);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }
    }
}