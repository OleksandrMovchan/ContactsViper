﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.RightView")]
    class RightView : LinearLayout, IViewContact
    {
        private LayoutInflater _inflater;
        //IPresenterContact _presenter;
        //public IPresenterContact Presenter { get => _presenter; set => _presenter = value; }

        public event Action OnImageClick;
        public event Action OnCardClick;

        public RightView(Context context) : base(context)
        {
        }

        public RightView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected RightView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.RightView, this, true);

            var card = FindViewById<CardView>(Resource.Id.rv_card);
            var photo = FindViewById<ImageView>(Resource.Id.rv_photo);

            card.Radius = 72;

            /*
            card.Click += delegate
            {
                _presenter.CardClicked();
            };

            photo.Click += delegate
            {
                _presenter.AvatarClicked();
            };
            */

            if (!photo.HasOnClickListeners)
            {
                photo.Click += Image_Click;
            }

            if (!card.HasOnClickListeners)
            {
                card.Click += Card_Click;
            }
        }

        private void Card_Click(object sender, EventArgs e)
        {
            OnCardClick?.Invoke();
        }

        private void Image_Click(object sender, EventArgs e)
        {
            OnImageClick?.Invoke();
        }

        public void SetName(string name)
        {
            var nameField = FindViewById<TextView>(Resource.Id.rv_name);
            nameField.Text = name;
        }

        public void SetNumber(string number)
        {
            var numberField = FindViewById<TextView>(Resource.Id.rv_number);
            numberField.Text = number;
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.rv_photo);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }


    }
}