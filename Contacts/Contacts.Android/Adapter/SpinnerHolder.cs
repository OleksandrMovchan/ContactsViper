﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace Contacts.Droid.Adapter
{
    class SpinnerHolder : RecyclerView.ViewHolder
    {
        public ProgressBar Spinner { get; set; }

        public SpinnerHolder(View itemView) : base(itemView)
        {
            Spinner = itemView.FindViewById<ProgressBar>(Resource.Id.hold_spinner);

        }
    }
}