﻿using System;
using System.IO;
using Contacts.Droid.Data;
using Contacts.Repo.SQLiteHelper;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidSQLite))]
namespace Contacts.Droid.Data
{
    class AndroidSQLite : ISQLite
    {
        public string GetDatabasePath(string filename)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, filename);
            return path;
        }
    }
}