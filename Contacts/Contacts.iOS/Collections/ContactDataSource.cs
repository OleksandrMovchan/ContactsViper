﻿using System;
using Contacts.iOS.Cells;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Foundation;
using UIKit;

namespace Contacts.iOS.Collections
{
	public class ContactDataSource : UICollectionViewDataSource
    {
		private IDataWrapper _data = new DataWrapper();
        private UINavigationController _navigationController;

        public ContactDataSource(UINavigationController navigationController)
        {
            _navigationController = navigationController;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.DequeueReusableCell(ContactCell.Key, indexPath) as ContactCell;
            int position = (int)indexPath.Item;
            cell.StartViper(_data, _navigationController);
            cell.SetId(position);

            return cell;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
			return _data == null ? 0 : _data.GetCount();
        }
	}
}
