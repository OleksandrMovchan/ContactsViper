﻿using System;
using Foundation;

namespace Contacts.iOS.Collections
{
    public class ImageDelegate
    {
		public event Action<NSIndexPath> Tapped;

        public void OnTapped(NSIndexPath indexPath)
        {
            Tapped?.Invoke(indexPath);
        }
    }
}
