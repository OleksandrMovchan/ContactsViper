﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Contacts.iOS.Views
{
    [Register ("ContactView")]
    partial class ContactView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _contactCellView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _number { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _photo { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_contactCellView != null) {
                _contactCellView.Dispose ();
                _contactCellView = null;
            }

            if (_name != null) {
                _name.Dispose ();
                _name = null;
            }

            if (_number != null) {
                _number.Dispose ();
                _number = null;
            }

            if (_photo != null) {
                _photo.Dispose ();
                _photo = null;
            }
        }
    }
}