using Contacts.iOS.Collections;
using Contacts.Viper.ContactsList.Interfaces;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Contacts.iOS.Views
{
	[DesignTimeVisible(true)]
	public partial class ContactView : UIView, IComponent, IViewContact
	{
		public event Action OnCardClick;
		public event Action OnImageClick;

		public event EventHandler Disposed;

		private ImageDelegate _imageDelegate;
		private NSIndexPath _indexPath;

		public ContactView(IntPtr handle) : base(handle)
		{
		}

		public ISite Site { get; set; }
		public ImageDelegate ImageDelegate { get => _imageDelegate; set => _imageDelegate = value; }
		public NSIndexPath IndexPath { get => _indexPath; set => _indexPath = value; }


		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			if (Site != null && Site.DesignMode) return;

			NSBundle.MainBundle.LoadNib("ContactView", this, null);

			InvokeOnMainThread(() =>
            {
				var frame = _contactCellView.Frame;
                frame.Height = Frame.Height;
                frame.Width = Frame.Width;
				_contactCellView.Frame = frame;
				AddSubview(_contactCellView);

            });

			_photo.Layer.CornerRadius = 40;
			_photo.ClipsToBounds = true;         

			_photo.UserInteractionEnabled = true;

			_photo.AddGestureRecognizer(new UITapGestureRecognizer(HandleActionPhoto));

			_contactCellView.AddGestureRecognizer(new UITapGestureRecognizer(HandleActionCell));

		}

		private void HandleActionCell(UIGestureRecognizer recognizer)
		{
			if (recognizer.State == UIGestureRecognizerState.Ended)
            {
                OnCardClick?.Invoke();
            }
		}

		private void HandleActionPhoto(UIGestureRecognizer recognizer)
		{
			if (recognizer.State == UIGestureRecognizerState.Ended)
			{
				OnImageClick?.Invoke();
			}
		}

		public void SetName(string name)
		{
			_name.Text = name;
		}

		public void SetNumber(string number)
		{
			_number.Text = number;
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_photo.Image = image;
		}
	}
}