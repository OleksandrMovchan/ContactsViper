﻿using System;
using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using Foundation;
using UIKit;

namespace Contacts.iOS.Cells
{
	public partial class ContactCell : UICollectionViewCell
	{
		public static readonly NSString Key = new NSString("ContactCell");
		public static readonly UINib Nib;

		IInteractorContact _interactor;
      
		//private ImageDelegate _imageDelegate;
        
		//public ImageDelegate ImageDelegate { get => _imageDelegate; set => _imageDelegate = value; }
		//public IInteractorContact Interactor { get => _interactor; set => _interactor = value; }

		static ContactCell()
		{
			Nib = UINib.FromName("ContactCell", NSBundle.MainBundle);
		}

		protected ContactCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
		}

        public void StartViper(IDataWrapper data, UINavigationController navigationController)
        {
            IRouter router = new IOSRouter(navigationController);

            IPresenterContact presenter = new PresenterContact(_contactView, router);

            _interactor = new InteractorContact(data);
            _interactor.Presenter = presenter;
        }

        public void SetId(int id)
		{
			_interactor.SetId(id);
		}
	}
}
