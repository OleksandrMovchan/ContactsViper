﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Contacts.iOS.Cells
{
    [Register ("ContactCell")]
    partial class ContactCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Contacts.iOS.Views.ContactView _contactView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_contactView != null) {
                _contactView.Dispose ();
                _contactView = null;
            }
        }
    }
}