﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Contacts.iOS.Controllers
{
    [Register ("InfoViewController")]
    partial class InfoViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _infoName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _infoNumber { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _infoPhoto { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_btnBack != null) {
                _btnBack.Dispose ();
                _btnBack = null;
            }

            if (_infoName != null) {
                _infoName.Dispose ();
                _infoName = null;
            }

            if (_infoNumber != null) {
                _infoNumber.Dispose ();
                _infoNumber = null;
            }

            if (_infoPhoto != null) {
                _infoPhoto.Dispose ();
                _infoPhoto = null;
            }
        }
    }
}