using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace Contacts.iOS.Controllers
{
    public partial class InfoViewController : UIViewController, IViewInfo
    {
        private int _id;

        public event Action OnEditClick;
        public event Action OnDeleteClick;

        public int Id { get => _id; set => _id = value; }

        public InfoViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

            IDataWrapper data = new DataWrapper();

            IRouter router = new IOSRouter(NavigationController);

            IPresenterInfo presenter = new PresenterInfo(this, router);

            IInteractorInfo interactor = new InteractorInfo(data);
            interactor.Presenter = presenter;

            interactor.SetId(_id);
            
			_btnBack.TouchUpInside += delegate {
                presenter.BackClicked();
            };
		}

		public void SetName(string name)
		{
			_infoName.Text = name;
		}

		public void SetNumber(string number)
		{
			_infoNumber.Text = number;
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_infoPhoto.Image = image;
		}
	}
}