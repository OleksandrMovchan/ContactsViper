using Contacts.iOS.Cells;
using Contacts.iOS.Collections;
using System;
using UIKit;
using CoreGraphics;

namespace Contacts.iOS.Controllers
{
    public partial class CollectionViewController : UICollectionViewController
    {
		private ContactDelegate _contactDelegate;
        private UIImageView _imageView;

        public CollectionViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _imageView = new UIImageView(new CGRect(0, 0, View.Bounds.Width, View.Bounds.Height));
            _imageView.ContentMode = UIViewContentMode.ScaleToFill;


            _collectionView.RegisterNibForCell(ContactCell.Nib, ContactCell.Key);
            _contactDelegate = new ContactDelegate();
            _collectionView.Delegate = _contactDelegate;
			_collectionView.DataSource = new ContactDataSource(NavigationController);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
	}
}