using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsPhoto;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;
using System;
using UIKit;

namespace Contacts.iOS
{
	public partial class ImageViewController : UIViewController, IViewPhoto
    {
        private int _id;

        public int Id { get => _id; set => _id = value; }

        public ImageViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
            
			IDataWrapper data = new DataWrapper();
            IRouter router = new IOSRouter(NavigationController);

			IPresenterPhoto presenter = new PresenterPhoto(this, router);

            IInteractorPhoto interactor = new InteractorPhoto(data);
            interactor.Presenter = presenter;

            interactor.SetId(_id);
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_photoCard.Image = image;
		}
	}
}